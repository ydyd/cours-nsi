---
author: Votre nom
title: Page SQL
---

Lien pour la documentation : 

[doc pour sql](https://epithumia.github.io/mkdocs-sqlite-console/usage/#afficher-la-consoleide){:target="_blank" }

## Ecrire votre propre code, puis l'exécuter

{!{ sqlide titre="Votre code SQL" espace="exercices_sql"}!}

## On a pré-rempli : 

{!{ sqlide titre="tester SQL avec du code pré-saisi" sql="avec_SQL/sql/code_1.sql" espace="exercices_sql"}!}

## Utilisation d'une base de donnée : 

{!{ sqlide titre="essai avec une base de donnée et du code pré-saisi"  base="avec_SQL/bases/Livres.db" sql="avec_SQL/sql/code_editeur.sql" espace="exercices_sql"}!}

## Initialisation d'un IDE avec du code caché :

base et init ne peuvent pas être utilisés en même temps

Code de Init : 

```sql title="ce qui est caché"
DROP TABLE IF EXISTS employees;
CREATE TABLE employees
(
    id          integer,
    name        text,
    designation text,
    manager     integer,
    hired_on    date,
    salary      integer,
    commission  float,
    dept        integer
);

INSERT INTO employees VALUES (1,'JOHNSON','ADMIN',6,'1990-12-17',18000,NULL,4);
INSERT INTO employees VALUES (2,'HARDING','MANAGER',9,'1998-02-02',52000,300,3);
INSERT INTO employees VALUES (3,'TAFT','SALES I',2,'1996-01-02',25000,500,3);
INSERT INTO employees VALUES (4,'HOOVER','SALES I',2,'1990-04-02',27000,NULL,3);
INSERT INTO employees VALUES (5,'LINCOLN','TECH',6,'1994-06-23',22500,1400,4);
INSERT INTO employees VALUES (6,'GARFIELD','MANAGER',9,'1993-05-01',54000,NULL,4);
```


{!{ sqlide titre="Init + Code" init="avec_SQL/sql/init_1.sql" sql="avec_SQL/sql/code_sql.sql" }!}