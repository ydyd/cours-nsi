# --- PYODIDE:env --- #

import matplotlib         # Indispensable (provoque la déclaration de PyodidePlot)
plt1 = PyodidePlot('cible_1')  
plt2 = PyodidePlot('cible_2')

# --- PYODIDE:code --- #

xs = [-3 + k * 0.1 for k in range(61)]
ys = [x**2 for x in xs]
plt1.plot(xs, ys, "r-")
plt1.grid()  # Optionnel : pour voir le quadrillage
plt1.axhline()  # Optionnel : pour voir l'axe des abscisses
plt1.axvline()  # Optionnel : pour voir l'axe des ordonnées
plt1.title("La fonction carré")
plt1.show()

xs = [-2 + k * 0.1 for k in range(41)]
ys = [x**3 for x in xs]
plt2.plot(xs, ys, "r-")
plt2.grid()  # Optionnel : pour voir le quadrillage
plt2.axhline()  # Optionnel : pour voir l'axe des abscisses
plt2.axvline()  # Optionnel : pour voir l'axe des ordonnées
plt2.title("La fonction cube")
plt2.show()
