---
author: Votre nom
title: Exercice avec du code caché et des saisies
---

???+ question "Je joue contre l'ordinateur"

    Question 1.   

    Exécuter le programme du jeu du nombre mystère.
    Faire quelques parties, expliquer la stratégie de l'ordinateur pour trouver le nombre mystère.

    {{ IDE('exo') }} 

    ??? success "Solution"

        A chaque fois, il se place au milieu.